class Shape:
    def __init__(self, name):
        self.name = name

    def printout(self):
        print(self.name)


class Components:
    def __init__(self):
        self.a_list = []

    def add(self, shape):
        self.a_list.append(shape)

    def printout(self):
        print("Num of elements:", len(self.a_list))
        for elem in self.a_list:
            elem.printout()


s1 = Shape("Circle")
s2 = Shape("Square")
s3 = Shape("Rectangle")

c = Components()
c.add(s1)
c.add(s2)
c.add(s3)
c.printout()
