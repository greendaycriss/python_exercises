import time


# O(n) complexity for a sequential search
# Returns position if exists, otherwhise: None
def linear_search(num, array):
    for x in array:
        if x == num:
            return x


# O(log n) complexity for a interval search
# Returns postion if exists, otherwise: None
def binary_search(num, array):
    middle = round(len(array)/2)
    val = array[middle]

    if num <= val:
        for x in range(middle):
            if x == num:
                return x
    else:
        for x in range(middle, len(array)):
            if x == num:
                return x


# Returns postion if exists, otherwise: None
def python_search(num, array):
    if num in array:
        return array.index(num)


def execute():
    # Sorted elements !
    num_of_elem = 100000000  # 100,000,000
    number = 99999999  # 999,999,999

    data = [x for x in range(num_of_elem)]

    start = time.time()
    linear_search(number, data)
    end = time.time()
    print("linear search took: %.3f" % (end - start))

    start = time.time()
    binary_search(number, data)
    end = time.time()
    print("binary search took: %.3f" % (end - start))

    start = time.time()
    python_search(number, data)
    end = time.time()
    print("python search took: %.3f" % (end - start))


execute()
