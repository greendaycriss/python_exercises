import time
import random


# data = [4, 3, 2, 1, 0]
# data = [random.randrange(-100, 100) for x in range(10000)]
data = [x for x in reversed(range(0, 10000))]


# O(n*n) time complexity
# returns a new sorted list
# Bubble Sort took: 8.864 seconds
def bubble_sort(data):
    array = data.copy()
    n = len(data)
    for y in range(n-1):
        for x in range(n-1-y):
            if array[x] > array[x+1]:
                temp = array[x+1]
                array[x+1] = array[x]
                array[x] = temp
    return array


# O(n*n) time complexity
# returns a new sorted list
# Bubble Sort took: 8.864 seconds
def bubble_sort_optimized(data):
    array = data.copy()
    n = len(data)
    for y in range(n-1):
        swapped = False
        for x in range(n-1-y):
            if array[x] > array[x+1]:
                array[x+1], array[x] = array[x], array[x+1]
                swapped = True
        if swapped != True:
            break
    return array


# O(n*n) complexity
# returns a new sorted list
# Insert Sort took: 5.256 seconds
def insert_sort(data):
    array = data.copy()
    n = len(data)

    for x in range(1, n):
        key = array[x]

        # Move elements of arr[0..x-1], that are
        # greater than key, to one position ahead
        # of their current position
        y = x - 1
        while y >= 0 and key < array[y]:
            array[y+1] = array[y]
            y = y - 1
        array[y+1] = key

    return array


def execute():
    start = time.time()
    bubble_sort(data)
    end = time.time()
    print("Bubble Sort took: %.3f seconds" % (end - start))

    start = time.time()
    bubble_sort_optimized(data)
    end = time.time()
    print("Bubble Sort Optimized took: %.3f seconds" % (end - start))

    start = time.time()
    insert_sort(data)
    end = time.time()
    print("Insert Sort took: %.3f seconds" % (end - start))

    # Timsort is a hybrid stable sorting algorithm
    # derived from merge sort and insertion sort
    # Insert Sort took: 0.000 seconds
    start = time.time()
    array = data.copy()
    array.sort()
    end = time.time()
    print("Python Sort took: %.3f seconds" % (end - start))


execute()
