import json
import re


def fct1():
    myjson = '{"sku": "VA-43", "size": "5ft", "name": "Blue Sky",' \
             '"description": "A great product!"}'
    mydict = json.loads(myjson)

    for x in mydict.keys():
        if x != "description":
            mydict[x] = mydict[x].lower()
            mydict[x] = re.sub(r"[^a-zA-Z0-9_.]", "_", mydict[x])
            if len(mydict[x]) > 10:
                mydict[x] = mydict[x][:10].ljust(13, ".")

            if x == "size":
                index = mydict[x].index("ft")
                str_value = mydict[x][:index]
                str_value = str(float(str_value)*0.3048)
                mydict[x] = str_value

    # convert back to a JSON string
    myjson = json.dumps(mydict)
    print(myjson)


def fct2():
    n = 10
    p = 0
    for i in range(n):
        if n % 3 == 0:
            q = i + 4 if n < 40 else None
        else:
            q = i + 40
        p += float(q) / 1000
    print("q=", q)


def fct3():
    mystr = "<div>125mm x 8mm x 1212mm</div>"
    rez = re.search(r"<div>([0-9]{0,4})mm x ([0-9]{0,4})mm x"
                    r"([0-9]{0,4})mm</div>", mystr)
    print("rez=", rez)


def print_all(m):
    upper_bound = 0
    while True:
        for i in range(upper_bound):
            print(i)
        if upper_bound > m:
            break
        upper_bound += 1


fct1()
fct2()
fct3()
print_all(1)
print_all(5)
