
name = "This is a global variable"


def function():
    global name
    print(name)  # out: This is a global variable
    name = "Mary"

def function2(val, l):
    l.append(val)
    print(l)

function()
print(name)  # out: Mary

# function2(2)

mylist = [0, 1]
function2(7, mylist)

function2(9, mylist)

