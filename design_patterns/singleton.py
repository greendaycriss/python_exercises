class Singleton:
    # static variable
    __instance = None

    def __init__(self):
        if Singleton.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            Singleton.__instance = self

    # static method 
    # no self param
    def get_instance():
        return Singleton.__instance
    
    @staticmethod
    def get_instance():
        return Singleton.__instance

    # static method
    # no self param
    def printout():
        print(Singleton.num)


class Test:
    s1 = Singleton()
    print( Singleton.get_instance() )
    print( s1.__init__() )
    # s2 = Singleton.get_instance()
    # s3 = Singleton.get_instance()
    # print(s1)
    # print(s2)
    # print(s3)
