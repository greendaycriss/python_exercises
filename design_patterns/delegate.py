class Shape:
    pass


class Circle(Shape):
    def printout(self):
        print("Circle method")


class Square(Shape):
    def printout(self):
        c = Circle()
        c.printout()


s = Square()
s.printout()