
# LIST - MUTABLE INDEXABLE
def fct_list():
    mylist = [1, 3, 4, 3, 0, -20]

    # create using constructor
    mylist = list((1, 3, 4, 3, 0, -20))

    print(mylist)  # out: [1, 3, 4, 3, 0, -20]
    print(mylist[0])  # out: 1
    print(mylist[-1])  # out: -20
    print(mylist[-2])  # out: 0
    print(mylist[1:3])  # out: [3, 4]

    print(type(mylist))  # out: <class 'list'>
    print(len(mylist))  # out: 6
    print()


# TUPLE - IMMUTABLE INDEXABLE
def fct_tuple():
    # no changeable
    mytuple = ('Monday', 'Tuesday', -23, 'Friday', 'Thursday', 'Friday')

    # create tuple using constructor
    mytuple = tuple(('Monday', 'Tuesday', -23, 'Friday', 'Thursday'))

    mytuple[0] == ''  # cannot be changed !

    print(mytuple)
    print(mytuple[-1])  # access last element
    print(type(mytuple))
    print(len(mytuple))


# SET - MUTABLE NOT INDEXABLE NO DUPLICATES
def fct_set():
    myset = {'banana', 'cake', 1, 'banana'}

    # create set using constructor
    myset = set(('banana', 'cake', 1, 'banana'))

    # print(myset[0])  # cannot be accessed by index !
    # print(myset['banana'])  # cannot be accessed by index !

    myset.add("orange")

    myset.pop()  # remove last element but it's random

    myset.remove('cake')  # throws error if not exists

    myset.discard('cake')


# DICTIONARY - MUTABLE INDEXABLE
def fct_dict():
    mydict = {'color': 'blue', 2: 'batoxan'}

    # create using constructor
    mydict = dict(color='blue', a_type='batoxan')

    # create using mapping
    mydict = dict({'color': 'blue', 2: 'batoxan'})

    # As of Python version 3.7, dictionaries are ordered !!
    mydict[2] = 'muian'  # change value
    mydict['type'] = 'java'  # add element
    mydict.pop('color')  # remove element

    mydict[2]  # get value by key
    mydict.get(2)  # get value by key

    mydict.keys()  # get keys
    mydict.values()  # get values


fct_list()
fct_tuple()
fct_set()
fct_dict()
