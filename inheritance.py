class Person:
    def __init__(self, name):
        self.name = name

    def _print_to_console(self):
        print("Name is: %s" % self.name)


class Student(Person):
    def __init__(self, name, age):
        super().__init__(name)
        self.age = age

    def _print_to_console(self):
        print("Age for %s is: %s" % (self.name, self.age))


p = Person("Guru")
p._print_to_console()

s = Student("Butoi", 35)
s._print_to_console()
