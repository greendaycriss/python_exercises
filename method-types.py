class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    
    # instance method
    def method1(self):
        return Person("Papi", "80")
    
    # static method
    def method2(a, b):
        return a + b
    
    # class method
    @classmethod
    def method3(cls):
        return cls("Ticu", 100)


class Test:
    person = Person("Guru", "29")
    print(person.name)
    print(person.age)
    print()

    person1 = person.method1()
    print(person1.name)
    print(person1.age)
    print()

    Person.method2(10, 10)
    person2 = person.method3()
    print(person2.name)
    print(person2.age)
